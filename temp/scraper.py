import requests
from bs4 import BeautifulSoup
url = 'https://www.detik.com/terpopuler'

res = requests.get(url)
soup = BeautifulSoup(res.text, 'html.parser')

# proses scraping
title = soup.find('h3', attrs={'class':'media__title'}).text.strip()

# cetak hasil scraping
print(title) # <- mengambil judul
